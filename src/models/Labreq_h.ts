import * as Knex from 'knex';
import * as moment from 'moment-timezone';

export class LabreqHeaderModel {
  tableName = 'Labreq_h';
  selecteView: string = ` * from Labreq_h `;

  async list(db: Knex, req: any) {
    let info: any = req.body;
    let xlimit = 100;
    let xorderby = "req_no";

    for (const property in info) { if (info[property] === "") { delete info[property]; } }

    if (info.limit) { xlimit = info.limit; }
    if (info.orderby) { xorderby = info.orderby; }

    let criteria = " Where ";

    if (info.req_no) { criteria += ` req_no = '${info.req_no}' AND `; }
    if (info.HosCode) { criteria += ` HosCode = '${info.HosCode}' AND `; }
    if (info.username) { criteria += ` username like  '%${info.username}%' AND `; }
    if (info.patient_id) { criteria += ` patient_id like  '%${info.patient_id}%' AND `; }
    if (info.cid) { criteria += ` cid like  '%${info.cid}%' AND `; }
    if (info.lconfirm) { criteria += ` lconfirm like  '%${info.lconfirm}%' AND `; }
    if (info.authFlag) { criteria += ` authFlag like  '%${info.authFlag}%' AND `; }

    criteria += " 1=1";


    if (info.req_date_start == "" || info.req_date_end == "") {
      delete info.req_date_start;
      delete info.req_date_end;
    }
    
    if (info.req_date_start && info.req_date_end) { criteria += ` (req_date between  '${info.req_date_start}' AND  '${info.req_date_end}' ) AND `; }


    let sql = `select top ${xlimit}  ${this.selecteView} ${criteria} order by ${xorderby} desc `;
    let datas = await db.raw(sql);
    const result: any = {
        ok: true,
        text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
        rows: datas
    }
    return result;
  }

  async select_id(db: Knex, id: any) {
    let sql = `select ${this.selecteView} where req_no = ${id}`;
    const rs = await db.raw(sql);
    return rs;
  }

  async save(db: Knex, data: any) {

    // console.log(data);
    
    let rs_req_no: any  = await db.raw(`SELECT
    RTRIM(CASE
     WHEN
      yr = CONVERT ( VARCHAR, CONVERT ( INT, SUBSTRING ( CONVERT ( VARCHAR, getdate(), 112 ), 3, 2 )) + 43 ) THEN
       concat (
        hos_id,
        yr,
        dbo.padc ( CONVERT ( VARCHAR, CONVERT ( FLOAT, running ) + 1 ), '0', 6 )) ELSE concat (
        hos_id,
        CONVERT ( VARCHAR, CONVERT ( INT, SUBSTRING ( CONVERT ( VARCHAR, getdate(), 112 ), 3, 2 )) + 43 ),
        '000001' 
       ) 
      END ) AS runno,
     RTRIM( CASE
       WHEN yr = CONVERT ( VARCHAR, CONVERT ( INT, SUBSTRING ( CONVERT ( VARCHAR, getdate(), 112 ), 3, 2 )) + 43 ) THEN
       dbo.padc ( CONVERT ( VARCHAR, CONVERT ( FLOAT, running ) + 1 ), '0', 6 ) ELSE '000001' 
      END) AS runnoupd,
     RTRIM( CASE
       WHEN yr = CONVERT ( VARCHAR, CONVERT ( INT, SUBSTRING ( CONVERT ( VARCHAR, getdate(), 112 ), 3, 2 )) + 43 ) THEN
       yr ELSE CONVERT ( VARCHAR, CONVERT ( INT, SUBSTRING ( CONVERT ( VARCHAR, getdate(), 112 ), 3, 2 )) + 43 ) 
      END) AS yrupdate 
     FROM
      lookup_Hoscode 
    WHERE
     hcode = '${data.hoscode}'`);

    let req_no:any;

    // let y:any = moment.tz(new Date(), "Asia/Bangkok").format('YYYY');
    // console.log(rs_req_no[0].runno);
    // console.log(rs_req_no);


    // let y_:any = +y + 543;
    // console.log(y_);
    
    // let x:string = `${y_}`;

    // console.log(x);
    
    // let year  = x.substring(4, 2);
    // let req_no_  = rs_req_no[0].req_no.substring(2, 0);

    // let _req_no_:any;

    // console.log("year",year);
    // console.log("req_no_",req_no_);
    
    if(rs_req_no[0].runno){
      delete data.req_no;

      req_no = rs_req_no[0].runno;
      data.req_no = req_no;
    }

    // console.log(data);
    
    for (const property in data) { if (data[property] === "") { delete data[property]; } }
    let saved_rs = await db(this.tableName)
      .insert(data);
    let dat = saved_rs;
    let datas: any  = await db.raw(`select ${this.selecteView} where req_no = '${req_no}'`);

    if(datas){
      // let HosCode = data.hoscode;
      let info = {
        running:rs_req_no[0].runnoupd,
        yr:rs_req_no[0].yrupdate
      }
      // console.log(info);
      
      let lk_hoscode:any = await db(`lookup_Hoscode`)
      .where("hcode", data.hoscode)
      .update(info);
  
    }

    const result: any = {
        ok: true,
        text: "บันทึกข้อมูลเรียบเสร็จร้อยแล้ว",
        rows: datas
      };
    return result;
  }


  async update(db: Knex, id: any, data: any) {
    for (const property in data) { if (data[property] === "") { delete data[property]; } }
    let sql = `select ${this.selecteView} where req_no = ${id}`;
    let rows = await db(this.tableName)
        .where("req_no", id)
        .update(data);
      let datas = await db.raw(sql);
      const result: any = {
        ok: true,
        text: "แก้ไขข้อมูลเรียบเสร็จร้อยแล้ว",
        rows: datas
      };
      return result;
  }

  delete(db: Knex, id: any) {
    return db(this.tableName)
      .where('req_no', id)
      .del();
  }

}