import * as Knex from 'knex';

export class LookupListtestModel {
  tableName = 'lookup_listtest';
  selecteView: string = ` * from lookup_listtest `;

  async list(db: Knex, req: any) {
    let info: any = req.body;
    let xlimit = 100;
    let xorderby = "id";

    for (const property in info) { if (info[property] === "") { delete info[property]; } }

    if (info.limit) { xlimit = info.limit; }
    if (info.orderby) { xorderby = info.orderby; }

    let criteria = " Where ";

    if (info.id) { criteria += ` id = '${info.id}' AND `; }
    if (info.code) { criteria += ` code = '${info.code}' AND `; }
    if (info.name) { criteria += ` name = '${info.name}' AND `; }
    if (info.component) { criteria += ` component like  '%${info.component}%' AND `; }

    criteria += " 1=1";


    let sql = `select top ${xlimit}  ${this.selecteView} ${criteria} order by ${xorderby} desc `;
    let datas = await db.raw(sql);
    const result: any = {
        ok: true,
        text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
        rows: datas
    }
    return result;
  }

  async select_id(db: Knex, id: any) {
    let sql = `select ${this.selecteView} where id = ${id}`;
    const rs = await db.raw(sql);
    return rs;
  }

  async save(db: Knex, data: any) {
    for (const property in data) { if (data[property] === "") { delete data[property]; } }
    let saved_rs = await db(this.tableName)
      .insert(data);
    // let dat = saved_rs;
    // let datas: any  = await db.raw(`select ${this.selecteView} where id = ${dat}`);
    const result: any = {
        ok: true,
        text: "บันทึกข้อมูลเรียบเสร็จร้อยแล้ว",
        // rows: datas
      };
    return result;
  }


  async update(db: Knex, id: any, data: any) {
    for (const property in data) { if (data[property] === "") { delete data[property]; } }
    let sql = `select ${this.selecteView} where id = ${id}`;
    let rows = await db(this.tableName)
        .where("id", id)
        .update(data);
      let datas = await db.raw(sql);
      const result: any = {
        ok: true,
        text: "แก้ไขข้อมูลเรียบเสร็จร้อยแล้ว",
        rows: datas
      };
      return result;
  }

  delete(db: Knex, id: any) {
    return db(this.tableName)
      .where('id', id)
      .del();
  }

}