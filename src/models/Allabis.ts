import * as Knex from 'knex';

export class AllabisModel {

  async select(db: Knex, req: any) {
    let info: any = req.body;
   
    let criteria = " Where ";

    if (info.req_no) { criteria += ` h.req_no = '${info.req_no}' AND `; }
    if (info.HIS_HN) { criteria += ` p.HIS_HN  = '${info.HIS_HN}' AND `; }
    if (info.cid) { criteria += ` p.cid  = '${info.cid}' AND `; }
    if (info.cmdSent) { criteria += ` h.cmdSent  = '${info.cmdSent}' AND `; }
    if (info.HosCode) { criteria += ` h.HosCode = '${info.HosCode}' AND `; }
    if (info.txtSearch) { criteria += ` p.HIS_HN = '${info.txtSearch}' OR p.name like '%${info.txtSearch}%' OR p.lname like '%${info.txtSearch}%' AND `; }

    if (info.req_date_start == "" || info.req_date_end == "") {
        delete info.req_date_start;
        delete info.req_date_end;
      }
      
      if (info.req_date_start && info.req_date_end) { criteria += ` ( h.req_date between '${info.req_date_start}'  AND  '${info.req_date_end}' ) AND `; }
    
    criteria += "isnull(cmdSent,'') <> '1' and isnull(l.Provider,'') = 'ALLABIS' GROUP BY h.req_no, p.HIS_HN ,t.prefix_type_name, p.name,p.lname,p.sex,p.cid ,h.OriginCode,p.birth,h.req_date,h.req_time,h.OriginName,h.HosCode,hc.hos_name,h.Clientid_IP";

    let sql = `
        select
        h.req_no as labno
        ,p.HIS_HN as patientid
        ,null as vn
        ,t.prefix_type_name as prefixname
        ,p.name as firstname
        ,null as middlename
        ,p.lname as lastname
        ,convert(char,p.birth,112) as dob
        ,p.sex as gender
        ,p.cid as idcard
        ,null as mobilephone
        ,concat(dbo.ymd2cymd(h.req_date),h.req_time,'00') as registerdatetime
        ,null as locationcode
        ,null as locationname
        ,null as visittype
        ,h.OriginCode as origincode
        ,h.OriginName as originname
        ,null as doctorcode
        ,null as doctorname
        ,null as rightcode
        ,null as rightname
        ,'R' as priority
        , h.HosCode as HospitalCode
        , hc.hos_name as HospitalName
        , h.Clientid_IP as clientid
        ,null as testorder
        from Labreq_h h
        left join PATIENT_Data p on(h.patient_id=p.patient_id)
        left join lookup_title t on(p.pre_name=t.prefix_type_id)
        left join Labreq_d d on(h.req_no=d.req_no)
        left join lookup_lab_speciment s on((d.SpecimenCode=s.Code collate Thai_BIN ))
        left join lookup_Hoscode hc on(p.HOSCode=hc.hcode)
        left join lookup_lab l on(d.lab_code=l.LC_Code)
        ${criteria}
        `;
    let datas = await db.raw(sql);
    const result: any = datas;
    return result;
  }
  async testorder(db: Knex, req_no: any) {
    // let req_no: any = req_no;
    let datas:any;
    let sql = `
          select
          d.lab_code as testcode
        ,	d.lab_name as testname
        ,	d.SpecimenCode as samplecode
        ,	s.Specimen_Name as samplename
        , 	Case When isnull(d.reverse_flag,'') = '' Then 'AO' When isnull(d.reverse_flag,'') = 'Y' Then 'CO' End  as actioncode
        ,	concat(dbo.ymd2cymd(d.[date]),d.[time],'00') as receivedatetime
        ,	concat(dbo.ymd2cymd(d.[date]),d.[time],'00') as collectiondatetime
        , h.cmdSent
        from Labreq_h h
        left join PATIENT_Data p on(h.patient_id=p.patient_id)
        left join lookup_title t on(p.pre_name=t.prefix_type_id)
        left join Labreq_d d on(h.req_no=d.req_no)
        left join lookup_lab_speciment s on((d.SpecimenCode=s.Code collate Thai_BIN ))
        left join lookup_Hoscode hc on(p.HOSCode=hc.hcode)
        left join lookup_lab l on(d.lab_code=l.LC_Code)
        where h.req_no='${req_no}' and isnull(cmdSent,'') <> '1' and isnull(l.Provider,'') = 'ALLABIS'
        `;
        datas = await db.raw(sql);

    const result: any = datas;
    return result;
  }


  async update(db: Knex, req: any) {
    let info: any = req.body;
    let datas:any;
    let datas_:any;
   if(info.req_no){
    let sql = `
    update Labreq_h set cmdSent='Y' where req_no = '${info.req_no}'
        `;
        datas = await db.raw(sql);
        datas_ = await db.raw(`select * from Labreq_h where req_no = '${info.req_no}'`);


   } else{
    datas_ = {text:"req_no ไม่มีข้อมูล"}
   }

    const result: any = datas_;


    return result;
  }

}