import * as Knex from 'knex';

export class LabresDetailModel {
  tableName = 'Labres_d';
  selecteView: string = ` * from Labres_d `;

  async list(db: Knex, req: any) {
    let info: any = req.body;
    let xlimit = 100;
    let xorderby = "id_resd";

    for (const property in info) { if (info[property] === "") { delete info[property]; } }

    if (info.limit) { xlimit = info.limit; }
    if (info.orderby) { xorderby = info.orderby; }

    let criteria = " Where ";

    if (info.Lab_id) { criteria += ` Lab_id = '${info.Lab_id}' AND `; }
    if (info.Req_no) { criteria += ` Req_no = '${info.Req_no}' AND `; }
    if (info.cid) { criteria += ` cid like  '%${info.cid}%' AND `; }
    if (info.ResultID) { criteria += ` ResultID like  '%${info.ResultID}%' AND `; }
    if (info.ResultName) { criteria += ` ResultName like  '%${info.ResultName}%' AND `; }
    if (info.LabCode) { criteria += ` LabCode like  '%${info.LabCode}%' AND `; }
    if (info.LabName) { criteria += ` LabName like  '%${info.LabName}%' AND `; }

    criteria += " 1=1";


    let sql = `select top ${xlimit}  ${this.selecteView} ${criteria} order by ${xorderby} desc `;
    let datas = await db.raw(sql);
    const result: any = {
        ok: true,
        text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
        rows: datas
    }
    return result;
  }

  async select_id(db: Knex, id: any) {
    let sql = `select ${this.selecteView} where id_resd = ${id}`;
    const rs = await db.raw(sql);
    return rs;
  }

  async save(db: Knex, data: any) {
    for (const property in data) { if (data[property] === "") { delete data[property]; } }
    let saved_rs = await db(this.tableName)
      .insert(data);
    // let dat = saved_rs;
    // let datas: any  = await db.raw(`select ${this.selecteView} where id_resd = ${dat}`);
    const result: any = {
        ok: true,
        text: "บันทึกข้อมูลเรียบเสร็จร้อยแล้ว",
        // rows: datas
      };
    return result;
  }

    async saveLabres_ds(db: Knex, data: any) {
    for (const property in data) { if (data[property] === "") { delete data[property]; } }
    let saved_rs = await db('Labres_ds')
      .insert(data);
    // let dat = saved_rs;
    // let datas: any  = await db.raw(`select ${this.selecteView} where id_resd = ${dat}`);
    const result: any = {
        ok: true,
        text: "บันทึกข้อมูลเรียบเสร็จร้อยแล้ว",
        // rows: datas
      };
    return result;
  }

  async update(db: Knex, id: any, data: any) {
    for (const property in data) { if (data[property] === "") { delete data[property]; } }
    let sql = `select ${this.selecteView} where id_resd = ${id}`;
    let rows = await db(this.tableName)
        .where("id_resd", id)
        .update(data);
      let datas = await db.raw(sql);
      const result: any = {
        ok: true,
        text: "แก้ไขข้อมูลเรียบเสร็จร้อยแล้ว",
        rows: datas
      };
      return result;
  }

  delete(db: Knex, id: any) {
    return db(this.tableName)
      .where('id_resd', id)
      .del();
  }



  async updateConfirm(db: Knex, req: any) {
    let info: any = req.body;
    let datas:any;
    let datas_:any;
   if(info.Req_no){
    let sql = `
    update Labreq_h set res_ok= 'Y' , lconfirm='Y' where Req_no = '${info.Req_no}'
        `;
        datas = await db.raw(sql);
        datas_ = await db.raw(`select * from Labreq_h where Req_no = '${info.Req_no}'`);


   } else{
    datas_ = {text:"Req_no ไม่มีข้อมูล"}
   }

    const result: any = datas_;


    return result;
  }


  async updateReady(db: Knex, req: any) {
    let info: any = req.body;
    let datas:any;
    let datas_:any;
   if(info.Req_no){
    let sql = `
        update Labreq_d set res_ready='Y' where Req_no = '${info.Req_no}'
        `;
        datas = await db.raw(sql);
        datas_ = await db.raw(`select * from Labreq_d where Req_no = '${info.Req_no}'`);

   } else{
    datas_ = {text:"Req_no ไม่มีข้อมูล"}
   }

    const result: any = datas_;


    return result;
  }

}