import * as Knex from 'knex';

export class LabResultModel {

  async select(db: Knex, req: any) {
    let info: any = req.body;
   
    let criteria = " Where ";

    if (info.req_date_start == "" || info.req_date_end == "") {
      delete info.req_date_start;
      delete info.req_date_end;
    }
    
    if (info.req_date_start && info.req_date_end) { criteria += ` ((Case When isnull(lh.cfm_date,'') <> '' Then lh.cfm_date Else lh.req_date End)  between  '${info.req_date_start}'  AND  '${info.req_date_end}' ) AND `; }



    if (info.req_no) { criteria += ` lh.req_no = '${info.req_no}' AND `; }
    if (info.HIS_HN) { criteria += ` p.HIS_HN  = '${info.HIS_HN}' AND `; }
    if (info.HIS_LABNO) { criteria += ` ld.HIS_LABNO  = '${info.HIS_LABNO}' AND `; }
    if (info.HosCode) { criteria += ` lh.HosCode = '${info.HosCode}' AND `; }
    if (info.txtSearch) { criteria += ` p.HIS_HN = '${info.txtSearch}' OR lh.HIS_LabNo = '${info.txtSearch}' OR p.name like '%${info.txtSearch}%' OR p.lname like '%${info.txtSearch}%' AND `; }

    criteria += "isnull(ld.res_ready,'')='Y' and 1=1";



    let sql = `
	SELECT NULL AS
	LabID,
	lh.req_no AS LabLN,
CASE
		WHEN isnull( lh.cfm_date, '' ) <> '' THEN
		concat ( lh.cfm_date, replace( lh.cfm_time, ':', '0' ) ) ELSE concat ( lh.req_date, lh.req_time ) 
	END AS OrderDate,
	p.HIS_HN AS HN,
	p.name AS PatientName,
	p.lname AS PatientLastName,
CASE
		WHEN p.sex= 'M' THEN
		'Male' ELSE 'Female' 
	END AS Sex,
	(dbo.los(dbo.ce2ymd(p.birth), lh.req_date, getdate() ) / 30) /12 AS Age  ,
	ls.id_resd AS ResultID,
	ld.HIS_LABNO AS HIS_LN,
	ls.LabCode1 AS LabCode,
	replace( replace( replace( ld.lab_name, ',', '-' ), '"', ' ' ), '''', ' ' ) AS LabName,
	ls.ResultName,
	replace( replace( replace( ls.Result, ',', '-' ), '"', ' ' ), '''', ' ' ) AS Result,
	ls.Unit AS Unit,
	ls.NormalRange AS NormalRange,
	ls.Flag AS Flag,
	concat ( ld.[date], ld.[time] ) AS AcceptDate,
	ls.ReportBy AS ReportBy,
	ls.ReportName AS ReportName,
	concat ( ls.ReportDate, ls.ReportTime ) AS ReportDate,
	ls.AuthoriseBy AS AuthoriseBy,
	ls.AuthoriseName AS AuthoriseName,
	concat ( ls.AuthoriseDate, ls.AuthoriseTime ) AS AuthoriseDate,
	ls.Comment AS Comment,
	ls.Method AS Method,
	ls.Specimen AS Specimen 
FROM
	Labreq_h lh
	INNER JOIN PATIENT_Data p ON ( lh.patient_id= p.patient_id )
	LEFT JOIN Labreq_d ld ON ( lh.req_no= ld.req_no )
	LEFT JOIN Labres_d ls ON ( ld.req_no= ls.req_no AND ld.lab_code= ls.LabCode1 COLLATE Thai_BIN ) 
    ${criteria} `;
    let datas = await db.raw(sql);
    const result: any = datas;
    return result;
  }

}