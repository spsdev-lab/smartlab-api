import * as Knex from 'knex';
import * as crypto from 'crypto'

export class UserModel {
  tableName = '[User]';
  selecteView: string = ` * from [User] `;

  async list(db: Knex, req: any) {
    let info: any = req.body;
    let xlimit = 100;
    let xorderby = "UserCode";

    for (const property in info) { if (info[property] === "") { delete info[property]; } }

    if (info.limit) { xlimit = info.limit; }
    if (info.orderby) { xorderby = info.orderby; }

    let criteria = " Where ";

    if (info.UserCode) { criteria += ` UserCode = '${info.UserCode}' AND `; }
    if (info.cid) { criteria += ` cid like  '%${info.cid}%' AND `; }
    if (info.FirstName) { criteria += ` FirstName like  '%${info.FirstName}%' AND `; }
    if (info.LastName) { criteria += ` LastName like  '%${info.LastName}%' AND `; }
    if (info.HosCode) { criteria += ` HosCode like  '%${info.HosCode}%' AND `; }

    criteria += " 1=1";


    let sql = `select top ${xlimit}  ${this.selecteView} ${criteria} order by ${xorderby} desc `;
    let datas = await db.raw(sql);
    const result: any = {
        ok: true,
        text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
        rows: datas
    }
    return result;
  }

  async select_id(db: Knex, id: any) {
    let sql = `select ${this.selecteView} where UserCode = '${id}'`;
    const rs = await db.raw(sql);
    return rs;
  }

  async save(db: Knex, data: any) {
    for (const property in data) { if (data[property] === "") { delete data[property]; } }
    let encPassword :any;
    if(data.Password){
    encPassword = crypto.createHash('md5').update(data.Password).digest('hex')
    delete data.Password;
    data.Password = encPassword;
    }
  

    let saved_rs = await db(this.tableName)
      .insert(data);
      // console.log(saved_rs);
      
    let dat = data.UserCode;
    let datas: any  = await db.raw(`select ${this.selecteView} where UserCode = '${dat}'`);
    const result: any = {
        ok: true,
        text: "บันทึกข้อมูลเรียบเสร็จร้อยแล้ว",
        rows: datas
      };
    return result
  }


  async update(db: Knex, id: any, data: any) {
    for (const property in data) { if (data[property] === "") { delete data[property]; } }

    let encPassword :any;
    if(data.Password){
    encPassword = crypto.createHash('md5').update(data.Password).digest('hex')
    delete data.Password;
    data.Password = encPassword;
    }

    let sql = `select ${this.selecteView} where UserCode = '${id}'`;
    let rows = await db(this.tableName)
        .where("UserCode", id)
        .update(data);
      let datas = await db.raw(sql);
      const result: any = {
        ok: true,
        text: "แก้ไขข้อมูลเรียบเสร็จร้อยแล้ว",
        rows: datas
      };
      return result;
  }

  delete(db: Knex, id: any) {
    return db(this.tableName)
      .where('UserCode', id)
      .del();
  }

}