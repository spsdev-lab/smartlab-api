import { LOADIPHLPAPI } from 'dns';
import * as Knex from 'knex';
import * as moment from 'moment-timezone';

export class PatientDataModel {
  tableName = 'PATIENT_Data';
  selecteView: string = ` * from PATIENT_Data `;

  async list(db: Knex, req: any) {
    let info: any = req.body;
    let xlimit = 100;
    let xorderby = "patient_id";

    for (const property in info) { if (info[property] === "") { delete info[property]; } }

    if (info.limit) { xlimit = info.limit; }
    if (info.orderby) { xorderby = info.orderby; }

    let criteria = " Where ";

    if(info.search_text){criteria += `(HC_HN like  '%${info.search_text}%' OR name like  '%${info.search_text}%' OR lname like  '%${info.search_text}%') AND `}
    if (info.patient_id) { criteria += ` patient_id = '${info.patient_id}' AND `; }
    if (info.cid) { criteria += ` cid = '${info.cid}' AND `; }
    if (info.HIS_HN) { criteria += ` HIS_HN = '${info.HIS_HN}' AND `; }
    if (info.passport) { criteria += ` passport = '${info.passport}' AND `; }
    if (info.name) { criteria += ` name like  '%${info.name}%' AND `; }
    if (info.lname) { criteria += ` lname like  '%${info.lname}%' AND `; }
    if (info.name_eng) { criteria += ` name_eng like  '%${info.name_eng}%' AND `; }
    if (info.lname_eng) { criteria += ` lname_eng like  '%${info.lname_eng}%' AND `; }
    if (info.telephone) { criteria += ` telephone =  '${info.telephone}' AND `; }
    if (info.province) { criteria += ` province = '${info.province}' AND `; }
    if (info.district) { criteria += ` district = '${info.district}' AND `; }
    if (info.HOSCode) { criteria += ` HOSCode = '${info.HOSCode}' AND `; }

    criteria += " 1=1";


    let sql = `select top ${xlimit}  ${this.selecteView} ${criteria} order by ${xorderby} desc `;
    let datas = await db.raw(sql);
    // let totle = db(this.tableName).count();
    // console.log(totle);
    
    const result: any = {
        ok: true,
        text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
        // count:totle,
        rows: datas
    }
    return result;
  }

  async select_id(db: Knex, id: any) {
    let sql = `select ${this.selecteView} where patient_id = ${id}`;
    const rs = await db.raw(sql);
    return rs;
  }

  async select_patient_id(db: Knex) {
    let sql = `SELECT TOP 1 ${this.selecteView} ORDER BY patient_id DESC `;
    const rs = await db.raw(sql);
    return rs;
  }

  async save(db: Knex, data: any) {

    let rs_patient_id: any  = await db.raw(`SELECT TOP 1 patient_id from ${this.tableName} ORDER BY patient_id DESC`);
    let patient_id:any;

    // console.log(rs_patient_id[0].patient_id);
    

    for (const property in data) { if (data[property] === "") { delete data[property]; } }
    delete data.patient_id;
    patient_id = +rs_patient_id[0].patient_id + 1;
    data.patient_id =patient_id;
    
    data.date_add = moment.tz(new Date(), "Asia/Bangkok").format('YYYY-MM-DD');
    data.date_sent = moment.tz(new Date(), "Asia/Bangkok").format('YYYY-MM-DD');

    // console.log(data);
    
    let saved_rs = await db(this.tableName)
      .insert(data);
    // let dat = saved_rs;
    // console.log(dat);
    

    let datas: any  = await db.raw(`select ${this.selecteView} where patient_id = ${patient_id}`);
    const result: any = {
        ok: true,
        text: "บันทึกข้อมูลเรียบเสร็จร้อยแล้ว",
        rows: datas
      };
    return result
  }


  async update(db: Knex, id: any, data: any) {
    for (const property in data) { if (data[property] === "") { delete data[property]; } }
    let sql = `select ${this.selecteView} where patient_id = ${id}`;
    let rows = await db(this.tableName)
        .where("patient_id", id)
        .update(data);
      let datas = await db.raw(sql);
      const result: any = {
        ok: true,
        text: "แก้ไขข้อมูลเรียบเสร็จร้อยแล้ว",
        rows: datas
      };
      return result;
  }

  delete(db: Knex, id: any) {
    return db(this.tableName)
      .where('patient_id', id)
      .del();
  }

}