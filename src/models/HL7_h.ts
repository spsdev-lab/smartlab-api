import * as Knex from 'knex';

export class HL7HModel {

  async select(db: Knex, req: any) {
    let info: any = req.body;
   
    let criteria = " Where ";

    if (info.SequenNo) { criteria += ` SequenNo = '${info.SequenNo}' AND `; }
    if (info.HosCode) { criteria += ` SentStatus = '${info.HosCode}' AND `; }

    if (info.DateMessage_start == "" || info.DateMessage_end == "") {
      delete info.DateMessage_start;
      delete info.DateMessage_end;
    }
    
    if (info.DateMessage_start && info.DateMessage_end) { criteria += ` substring(DateMessage,1,8) between  '${info.DateMessage_start}'  AND  '${info.DateMessage_end}' ) AND `; }

    criteria += "1=1";

    let sql = `
    select *
    from HL7_h
    ${criteria} `;
    let datas = await db.raw(sql);
    const result: any = datas;
    return result;
  }

  async HL7_d(db: Knex, SequenNo: any) {

    let sql = `select *
    from HL7_d
    where SequenNo = ${SequenNo}`;
    let datas = await db.raw(sql);

      const result: any = {
        ok: true,
        text: "แก้ไขข้อมูลเรียบเสร็จร้อยแล้ว",
        rows: datas
      };
      return result;
  }

  async update(db: Knex, req: any) {
    let info: any = req.body;

    let sql = `update HL7_h set SentStatus='Y' where SequenNo = ${info.SequenNo}`;
    await db.raw(sql);

    let sql1 = `select * from HL7_h where SequenNo = ${info.SequenNo}`;
    let datas = await db.raw(sql1);

      const result: any = {
        ok: true,
        text: "แก้ไขข้อมูลเรียบเสร็จร้อยแล้ว",
        rows: datas
      };
      return result;
  }

}