import * as Knex from 'knex';

export class lookupProfileLabModel {
  tableName = 'lookup_ProfileLab';
  selecteView: string = ` * from lookup_ProfileLab `;

  async select(db: Knex) {

    let sql = `select Code, Specimen_Name
    from lookup_lab_speciment
    where Enabled='YES'`;
    let datas = await db.raw(sql);
    const result: any = {
        ok: true,
        text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
        rows: datas
    }
    return result;
  }

  async list(db: Knex, req: any) {
    let info: any = req.body;
    let limit = info.limit || '';
    let xlimit = 100;
    let xorderby = "ProfileCode";

    for (const property in info) { if (info[property] === "") { delete info[property]; } }

    if (limit) { xlimit = info.limit; }
    if (info.orderby) { xorderby = info.orderby; }

    let criteria = " Where ";

    if (info.ProfileCode) { criteria += ` ProfileCode = '${info.ProfileCode}' AND `; }
    if (info.ProfileName) { criteria += ` ProfileName link '%${info.ProfileName}%' AND `; }
    if (info.GroupCode) { criteria += ` GroupCode = '${info.GroupCode}' AND `; }

    criteria += "isnull(CancelFlag,'') <> 'Y' AND 1=1";


    let sql = `select top ${xlimit}  ${this.selecteView} ${criteria} order by ${xorderby} desc `;
    let datas = await db.raw(sql);
    const result: any = {
        ok: true,
        text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
        rows: datas
    }
    return result;
  }
}