import * as Knex from 'knex';

export class LookupLabSpecimentModel {
  tableName = 'lookup_lab_speciment';
  selecteView: string = ` * from lookup_lab_speciment `;

  async select(db: Knex) {

    let sql = `select Code, Specimen_Name
    from lookup_lab_speciment
    where Enabled='YES'`;
    let datas = await db.raw(sql);
    const result: any = {
        ok: true,
        text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
        rows: datas
    }
    return result;
  }

  async list(db: Knex, req: any) {
    let info: any = req.body;
    let xlimit = 100;
    let xorderby = "Code";

    for (const property in info) { if (info[property] === "") { delete info[property]; } }

    // if (info.limit) { xlimit = info.limit; }
    if (info.orderby) { xorderby = info.orderby; }

    let criteria = " Where ";

    if (info.Code) { criteria += ` Code = '${info.Code}' AND `; }
    if (info.LabGroup) { criteria += ` LabGroup = '${info.LabGroup}' AND `; }
    if (info.Specimen_Name) { criteria += ` Specimen_Name like '%${info.Specimen_Name}%' AND `; }

    criteria += "Enabled='YES' AND 1=1";


    let sql = `select top ${xlimit}  ${this.selecteView} ${criteria} order by ${xorderby} desc `;
    let datas = await db.raw(sql);
    const result: any = {
        ok: true,
        text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
        rows: datas
    }
    return result;
  }

  async select_id(db: Knex, id: any) {
    let sql = `select ${this.selecteView} where id = ${id}`;
    const rs = await db.raw(sql);
    return rs;
  }

  async save(db: Knex, data: any) {
    for (const property in data) { if (data[property] === "") { delete data[property]; } }
    let saved_rs = await db(this.tableName)
      .insert(data);
    // let dat = saved_rs;
    // let datas: any  = await db.raw(`select ${this.selecteView} where id = ${dat}`);
    const result: any = {
        ok: true,
        text: "บันทึกข้อมูลเรียบเสร็จร้อยแล้ว",
        // rows: datas
      };
    return result;
  }


  async update(db: Knex, id: any, data: any) {
    for (const property in data) { if (data[property] === "") { delete data[property]; } }
    let sql = `select ${this.selecteView} where id = ${id}`;
    let rows = await db(this.tableName)
        .where("id", id)
        .update(data);
      let datas = await db.raw(sql);
      const result: any = {
        ok: true,
        text: "แก้ไขข้อมูลเรียบเสร็จร้อยแล้ว",
        rows: datas
      };
      return result;
  }

  delete(db: Knex, id: any) {
    return db(this.tableName)
      .where('id', id)
      .del();
  }

}