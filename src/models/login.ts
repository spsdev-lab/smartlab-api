import * as knex from 'knex';

export class LoginModel {

  constructor() { }

  login(db: knex, username: any, password: any) {
    return db('User')
      .where('UserCode', username)
      .where('Password', password)
  }

}
