import * as Knex from 'knex';

export class LabreqDetailModel {
  tableName = 'Labreq_d';
  selecteView: string = ` * from Labreq_d `;

  async list(db: Knex, req: any) {
    let info: any = req.body;
    let xlimit = 100;
    let xorderby = "id_reqd";

    for (const property in info) { if (info[property] === "") { delete info[property]; } }

    if (info.limit) { xlimit = info.limit; }
    if (info.orderby) { xorderby = info.orderby; }

    let criteria = " Where ";

    if (info.date_start == "" || info.date_end == "") {
      delete info.date_start;
      delete info.date_end;
    }
    
    if (info.date_start && info.date_end) { criteria += ` (date between  '${info.date_start}'::date  AND  '${info.date_end}'::date ) AND `; }


    if (info.id_reqd) { criteria += ` id_reqd = '${info.id_reqd}' AND `; }
    if (info.num_req) { criteria += ` num_req = '${info.num_req}' AND `; }
    if (info.patient_id) { criteria += ` patient_id like  '%${info.patient_id}%' AND `; }
    if (info.cid) { criteria += ` cid like  '%${info.cid}%' AND `; }
    if (info.lab_code) { criteria += ` lab_code like  '%${info.lab_code}%' AND `; }
    if (info.req_no) { criteria += ` req_no like  '%${info.req_no}%' AND `; }

    criteria += " 1=1";


    let sql = `select top ${xlimit}  ${this.selecteView} ${criteria} order by ${xorderby} desc `;
    let datas = await db.raw(sql);
    const result: any = {
        ok: true,
        text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
        rows: datas
    }
    return result;
  }

  async select_id(db: Knex, id: any) {
    let sql = `select ${this.selecteView} where id_reqd = ${id}`;
    const rs = await db.raw(sql);
    return rs;
  }

  async save(db: Knex, data: any) {
    for (const property in data) { if (data[property] === "") { delete data[property]; } }
    let saved_rs = await db(this.tableName)
      .insert(data);    

    // let dat = saved_rs;
    let datas: any  = await db.raw(`SELECT TOP 1 * from ${this.tableName} ORDER BY id_reqd DESC`);
    const result: any = {
        ok: true,
        text: "บันทึกข้อมูลเรียบเสร็จร้อยแล้ว",
        rows: datas
      };
    return result;
  }


  async update(db: Knex, id: any, data: any) {
    for (const property in data) { if (data[property] === "") { delete data[property]; } }
    let sql = `select ${this.selecteView} where id_reqd = ${id}`;
    let rows = await db(this.tableName)
        .where("id_reqd", id)
        .update(data);
      let datas = await db.raw(sql);
      const result: any = {
        ok: true,
        text: "แก้ไขข้อมูลเรียบเสร็จร้อยแล้ว",
        rows: datas
      };
      return result;
  }

  delete(db: Knex, id: any) {
    return db(this.tableName)
      .where('id_reqd', id)
      .del();
  }

}