import * as Knex from 'knex';

export class lookupProfileDetailModel {
  tableName = 'lookup_ProfileDetail';
  selecteView: string = ` * from lookup_ProfileDetail `;

  async select(db: Knex) {

    let sql = `select ProfileCode, LC_Code, CancelFlag
    from lookup_ProfileDetail
    where isnull(CancelFlag,'') <> 'Y' `;
    let datas = await db.raw(sql);
    const result: any = {
        ok: true,
        text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
        rows: datas
    }
    return result;
  }

  async list(db: Knex, req: any) {
    let info: any = req.body;
    let limit = info.limit || '';
    let xlimit = 100;
    let xorderby = "ProfileCode";

    for (const property in info) { if (info[property] === "") { delete info[property]; } }

    if (limit) { xlimit = info.limit; }
    if (info.orderby) { xorderby = info.orderby; }

    let criteria = " Where ";

    if (info.ProfileCode) { criteria += ` ProfileCode = '${info.ProfileCode}' AND `; }
    if (info.LC_Code) { criteria += ` LC_Code = '${info.LC_Code}' AND `; }

    criteria += "isnull(CancelFlag,'') <> 'Y' AND 1=1";


    let sql = `select top ${xlimit}  ${this.selecteView} ${criteria} order by ${xorderby} desc `;
    let datas = await db.raw(sql);
    const result: any = {
        ok: true,
        text: "การอ่านข้อมูลเสร็จเรียบร้อยแล้ว",
        rows: datas
    }
    return result;
  }
}