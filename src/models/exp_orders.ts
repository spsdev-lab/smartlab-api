import * as Knex from 'knex';

export class ExpOrdersModel {

  async select(db: Knex, req: any) {
    let info: any = req.body;
   
    let criteria = " Where ";

    if (info.HosCode) { criteria += ` h.HosCode = '${info.HosCode}' AND `; }

    if (info.req_date_start == "" || info.req_date_end == "") {
        delete info.req_date_start;
        delete info.req_date_end;
    }
      
    if (info.req_date_start && info.req_date_end) { criteria += ` (h.req_date between  '${info.req_date_start}'  AND  '${info.req_date_end}' ) AND `; }
  
    criteria += "isnull(h.lconfirm,'')='Y' AND 1=1";

    let sql = `select convert(varchar, concat(right(h.req_date,2),'-',substring(h.req_date,5,2),'-',left(h.req_date,4)), 103) as ReqDate
    , h.patient_id, p.HIS_HN as HN, concat(p.name,'  ',p.lname) as Name, concat(d.lab_code,' : ',l.Lab_Name) as LabName, l.Price_gov
    , Case When isnull(d.res_ready,'') = 'Y' Then 'OK' Else null End as ResReady
    from Labreq_d d
    left join Labreq_h h on(d.req_no=h.req_no and d.cid=h.cid)
    left join lookup_Hoscode lh on(h.HosCode=lh.hcode)
    left join PATIENT_Data p on(h.cid=p.cid collate Thai_BIN and h.HosCode=p.HOSCode collate Thai_BIN)
    left join lookup_lab l on(d.lab_code=l.LC_Code)
    ${criteria}
    order by h.req_date`;
    let datas = await db.raw(sql);
    const result: any = datas;
    return result;
  }

}