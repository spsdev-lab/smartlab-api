import * as Knex from 'knex';

export class CountStatusModel {

  async select(db: Knex, req: any) {
    let info: any = req.body;
   
    let criteria = " Where ";

    if (info.HosCode) { criteria += ` h.HosCode = '${info.HosCode}' AND `; }

    if (info.req_date_start == "" || info.req_date_end == "") {
        delete info.req_date_start;
        delete info.req_date_end;
    }
      
    if (info.req_date_start && info.req_date_end) { criteria += ` (h.req_date between  '${info.req_date_start}'  AND  '${info.req_date_end}' ) AND `; }
  
    criteria += "isnull(h.lconfirm,'')='Y' AND 1=1";

    let sql = `
        select count(h.req_no) as ALLS
        , sum(Case When isnull(h.res_ok,'')<>'Y' and isnull(h.lconfirm,'')='Y' then 1 Else 0 End) as ORDERS
        , null as PROCESS
        , null INCOMPLETE
        , sum(Case When isnull(h.res_ok,'')='Y' then 1 Else 0 End) as COMPLETE
        , null as UNCOMPLETE
        , sum(Case When isnull(h.lconfirm,'')<>'Y' then 1 Else 0 End) CANCEL
        from Labreq_h h
        ${criteria}
        `;
    let datas = await db.raw(sql);
    const result: any = datas;
    return result;
  }

}