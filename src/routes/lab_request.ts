import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify';
import { PatientDataModel } from '../models/patient'
import { LabreqHeaderModel } from '../models/Labreq_h'
import { LabreqDetailModel } from '../models/Labreq_d'

const patientDataModel = new PatientDataModel();
const labreqHeaderModel = new LabreqHeaderModel();
const labreqDetailModel = new LabreqDetailModel();

export default async function Lpurpose(fastify: FastifyInstance) {
  const db = fastify.mssql;

  fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
    reply.code(200).send({ message: 'Lpurpose  -->Fastify, RESTful API services! SPSLAB20220110' });
  });


  fastify.post('/', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const info:any = req.body;

    let info_patient:any;
    let info_labreqheader:any;
    let info_labreqdetail:any;
    
    let objectDatas:any = {};


    try {

        // insert patient data
        info_patient = info.patient_data;
        // console.log(info_patient);
        
        if(info_patient){
            let req_se:any ={};
            let i_s_patient:any = {
                limit:10,
                cid:info_patient.cid,
                HIS_HN:info_patient.HIS_HN,
                HOSCode:info_patient.HOSCode
            }
            req_se.body = i_s_patient;
            let rs_s_patient:any = await patientDataModel.list(db,req_se);
            info_labreqheader = info_patient.Labreq_h;

            // console.log(rs_s_patient);
            
            if(!rs_s_patient.rows[0]){
                let req_sa:any ={};
                req_sa = info_patient;
                delete req_sa.Labreq_h;
                let rs = await patientDataModel.save(db,req_sa);

                // console.log(rs.rows[0]);

                objectDatas.patient = rs.rows[0];
                
            }else{
              
                objectDatas.patient = rs_s_patient.rows[0]
                // console.log("patient :",objectDatas.patient );

            }
        }

        // insert labreq header & detail
        
        // console.log("info_labreqheader ",info_labreqheader);
        // console.log("patient ",objectDatas.patient);
        
        
        if(info_labreqheader[0] && objectDatas.patient){

            let labreqh_push:any = [];
            let labreqd_push:any = [];
            let labreqdetail:any = {};
            let labreqheader:any = [];
            for(const i of info_labreqheader){
              // console.log("info_labreqheader",i);
              
                delete i.patient_id;
                delete i.cid;
    
                i.patient_id = objectDatas.patient.patient_id
                i.cid = objectDatas.patient.cid
                let req_sa:any ={};
                // console.log(" i :",i);
                info_labreqdetail = i.Labreq_d;

                req_sa = i;
                delete req_sa.Labreq_d;
                //insert labreq-header
                let rs_s_i = await labreqHeaderModel.save(db,req_sa);
                // console.log(rs_s_i.rows[0]);
                
                labreqh_push.push(rs_s_i.rows[0]);

                

                // console.log("info_labreqdetail-11111:",info_labreqdetail);
                
                for(const e of info_labreqdetail){
                  // console.log("info_labreqdetail ",e);
                  
                    delete e.patient_id;
                    delete e.cid;
                    delete e.req_no;

                    e.patient_id = objectDatas.patient.patient_id
                    e.cid = objectDatas.patient.cid
                    e.req_no = rs_s_i.rows[0].req_no
                    let req_s_e:any ={};
                    req_s_e = e;
                    //insert labreq-detail
                    let rs_s_e = await labreqDetailModel.save(db,req_s_e);
                    // console.log(rs_s_e.rows[0]);
                    
                    labreqd_push.push(rs_s_e.rows[0]);

                }
                objectDatas.labreqdetail = labreqd_push;

            }
                
            objectDatas.labreqheader = labreqh_push;

        }
      // console.log(rs);
      reply.code(200).send(objectDatas);
    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
      });
    }
  });
}