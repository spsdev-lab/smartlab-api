import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify';
import { AllabisModel } from '../models/Allabis'

const fromImportModel = new AllabisModel();

export default async function Allabis(fastify: FastifyInstance) {
  const db = fastify.mssql;

  fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
    reply.code(200).send({ message: 'Allabis  -->Fastify, RESTful API services! SPSLAB20220110' });
  });


  fastify.post('/list', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    let info:any[]=[];
    try {
      var rs = await fromImportModel.select(db,req);
      // console.log(rs);
      for(const e of rs){
        let testorder:any[]=[];
      var rs_testorder = await fromImportModel.testorder(db,e.labno);
      // console.log(rs_testorder);
      for(const v of rs_testorder){
        let _testorder:any = {
          "testcode": v.testcode,
          "testname": v.testname,
          "samplecode": v.testname,
          "samplename": v.samplename,
          "actioncode": v.actioncode,
          "receivedatetime": v.receivedatetime,
          "collectiondatetime": v.collectiondatetime,
          "cmdSent": v.cmdSent
        }
        testorder.push(_testorder);
      }

        let x :any = e;           
        x.testorder = testorder;
        info.push(x);
      }

      reply.code(200).send(info);
    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
      });
    }
  });

  fastify.put('/', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    try {
      var rs = await fromImportModel.update(db,req);
      // console.log(rs);
      reply.code(200).send(rs);
    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
      });
    }
  });
}