import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify';
import { LabresDetailModel } from '../models/Labres_d'

const fromImportModel = new LabresDetailModel();

export default async function LabresDetail(fastify: FastifyInstance) {
  const db = fastify.mssql;

  fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
    reply.code(200).send({ message: 'LabresDetail  -->Fastify, RESTful API services! SPSLAB20220110' });
  });


  fastify.post('/list', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    try {
      var rs = await fromImportModel.list(db,req);
      // console.log(rs);
      reply.code(200).send(rs);
    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
      });
    }
  });

  fastify.post('/', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const info = req.body;
    const testResult = info.testResult;
    delete info.testResult;

    try {
      var rs:any = await fromImportModel.save(db, info);
      var rsLabres_ds:any;
      if(testResult){
        rsLabres_ds = await fromImportModel.saveLabres_ds(db, testResult);

      }

      reply.code(200).send({"Labres_d":rs,"Labres_ds":rsLabres_ds});
    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "การเพิ่มข้อมูลเกิดความผิดพลาด",
        error: error.message
      });
    }
  });

  //   UPDATE
  fastify.put('/:id', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const info = req.body;
    const id = req.params.id;
    try {
      var rs = await fromImportModel.update(db, id, info);
      reply.code(200).send(rs);
    } catch (error: any) {
      req.log.error(error);
      var rs = await fromImportModel.select_id(db, id);
      reply.code(500).send({
        ok: false,
        text: "การแก้ไขข้อมูลเกิดความผิดพลาด",
        error: error.message,
        rows: rs
      });
    }
  });


  fastify.delete('/:id', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const id = req.params.id;

    try {
      var rs = await fromImportModel.delete(db, id);
      reply.code(200).send({ ok: true, "text": `ลบข้อมูลเรียบร้อยแล้ว ${rs} รายการ`, rows: rs });
    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "การลบข้อมูลเกิดความผิดพลาด",
        error: error.message
      });
    }
  });

  fastify.put('/updateConfirm', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    try {
      var rs = await fromImportModel.updateConfirm(db, req);
      reply.code(200).send(rs);
    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "การแก้ไขข้อมูลเกิดความผิดพลาด",
        error: error.message,
      });
    }
  });

  fastify.put('/updateReady', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    try {
      var rs = await fromImportModel.updateReady(db, req);
      reply.code(200).send(rs);
    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "การแก้ไขข้อมูลเกิดความผิดพลาด",
        error: error.message,
      });
    }
  });

}