import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify';
import { lookupProfileDetailModel } from '../models/lookup_ProfileDetail'

const fromImportModel = new lookupProfileDetailModel();

export default async function lookup_ProfileDetail(fastify: FastifyInstance) {
  const db = fastify.mssql;

  fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
    reply.code(200).send({ message: 'lookup_ProfileDetail  -->Fastify, RESTful API services! SPSLAB20220110' });
  });

  fastify.post('/info', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    try {
      var rs = await fromImportModel.select(db);
      // console.log(rs);
      reply.code(200).send(rs);
    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
      });
    }
  });


  fastify.post('/list', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    try {
      var rs = await fromImportModel.list(db,req);
      // console.log(rs);
      reply.code(200).send(rs);
    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
      });
    }
  });
}