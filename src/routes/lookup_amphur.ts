import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify';
import { LookupAmphurModel } from '../models/lookup_amphur'

const fromImportModel = new LookupAmphurModel();

export default async function lookup_amphur(fastify: FastifyInstance) {
  const db = fastify.mssql;

  fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
    reply.code(200).send({ message: 'lookup_amphur  -->Fastify, RESTful API services! SPSLAB20220110' });
  });


  fastify.post('/list', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    try {
      var rs = await fromImportModel.list(db,req);
      // console.log(rs);
      reply.code(200).send(rs);
    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
      });
    }
  });

  fastify.post('/', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const info = req.body;

    try {
      var rs = await fromImportModel.save(db, info);
      reply.code(200).send(rs);
    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "การเพิ่มข้อมูลเกิดความผิดพลาด",
        error: error.message
      });
    }
  });

  //   UPDATE
  fastify.put('/:id', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const info = req.body;
    const id = req.params.id;
    try {
      var rs = await fromImportModel.update(db, id, info);
      reply.code(200).send(rs);
    } catch (error: any) {
      req.log.error(error);
      var rs = await fromImportModel.select_id(db, id);
      reply.code(500).send({
        ok: false,
        text: "การแก้ไขข้อมูลเกิดความผิดพลาด",
        error: error.message,
        rows: rs
      });
    }
  });


  fastify.delete('/:id', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const id = req.params.id;

    try {
      var rs = await fromImportModel.delete(db, id);
      reply.code(200).send({ ok: true, "text": `ลบข้อมูลเรียบร้อยแล้ว ${rs} รายการ`, rows: rs });
    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "การลบข้อมูลเกิดความผิดพลาด",
        error: error.message
      });
    }
  });



}