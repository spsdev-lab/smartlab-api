import * as crypto from 'crypto'

import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { LoginModel } from '../models/login'

export default async function login(fastify: FastifyInstance) {

  const db: any = fastify.mssql;
  const loginModel = new LoginModel()

  fastify.post('/', async (request: FastifyRequest, reply: FastifyReply) => {
    const body: any = request.body
    const username = body.username
    const password = body.password

    console.log(username);
    console.log(password);
    
    try {
      const encPassword = crypto.createHash('md5').update(password).digest('hex')
      const rs: any = await loginModel.login(db, username, encPassword)

      if (rs.length > 0) {
        const user: any = rs[0];
        const payload: any = user;

        const token = fastify.jwt.sign(payload)
        reply.send({ token })
      } else {
        reply.status(401).send({ ok: false,error: 'Login failed' , message: 'Login failed' })
      }


    } catch (error:any) {
      reply.status(500).send({ error: error.message ,message: error.message })
    }
  })

}
