import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify';
import { HL7HModel } from '../models/HL7_h'

const fromImportModel = new HL7HModel();

export default async function Lpurpose(fastify: FastifyInstance) {
  const db = fastify.mssql;

  fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
    reply.code(200).send({ message: 'HL7_h  -->Fastify, RESTful API services! SPSLAB20220110' });
  });

  fastify.post('/list', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    let info:any[]=[];

    try {
      var rs = await fromImportModel.select(db,req);
      // console.log(rs);
      for(const e of rs){
        let HL7_d:any[]=[];
      var rs_testorder = await fromImportModel.HL7_d(db,e.SequenNo);
      // console.log(rs_testorder.rows);
        let _HL7_d:any = rs_testorder.rows;
        let x :any = e;    
        x.HL7_d = _HL7_d;
        info.push(x);
      }


      reply.code(200).send(info);
    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
      });
    }
  });

  fastify.put('/', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    try {
      var rs = await fromImportModel.update(db,req);
      // console.log(rs);
      reply.code(200).send(rs);
    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "การแก้ไขข้อมูลเกิดความผิดพลาด",
        error: error.message
      });
    }
  });

}