import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify';
import { CountStatusModel } from '../models/count_status'

const fromImportModel = new CountStatusModel();

export default async function count_status(fastify: FastifyInstance) {
  const db = fastify.mssql;

  fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
    reply.code(200).send({ message: 'count_status  -->Fastify, RESTful API services! SPSLAB20220110' });
  });


  fastify.post('/list', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    try {
      var rs = await fromImportModel.select(db,req);
      // console.log(rs);
      reply.code(200).send(rs);
    } catch (error: any) {
      req.log.error(error);
      reply.code(500).send({
        ok: false,
        text: "การอ่านข้อมูลเกิดความผิดพลาด",
        error: error.message
      });
    }
  });
}