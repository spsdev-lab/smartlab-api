import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify';
import { LabResultModel } from '../models/lab_result'
import jwt_decode from "jwt-decode";

const fromImportModel = new LabResultModel();

export default async function Lpurpose(fastify: FastifyInstance) {
  const db = fastify.mssql;

  fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
    reply.code(200).send({ message: 'lab_result  -->Fastify, RESTful API services! SPSLAB20220110' });
  });


  fastify.post('/',{ preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const token = req.headers.authorization.split(' ')[1];
    const decoded:any = jwt_decode(token);
    const info:any = req.body;  
    
    
    console.log(info.HosCode);
    console.log(decoded.HosCode);
if(info.HosCode == decoded.HosCode){
  try {
    var rs = await fromImportModel.select(db,req);
    // console.log(rs);
    reply.code(200).send(rs);
  } catch (error: any) {
    req.log.error(error);
    reply.code(500).send({
      ok: false,
      text: "การอ่านข้อมูลเกิดความผิดพลาด",
      error: error.message
    });
  }
}else{
  reply.code(500).send({
    ok: false,
    text: "การอ่านข้อมูลเกิดความผิดพลาด",
    error: 'รหัสสถานบริการ ไม่ตรงกับ Token '
  });
}

  });
}