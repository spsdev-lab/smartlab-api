FROM keymetrics/pm2:14-alpine

LABEL maintainer="Thawatchai Seangduan <thawatchai.sea2@gmail.com>"

WORKDIR /home/tsexpress

RUN apk update && apk upgrade && apk add --no-cache alpine-sdk git \
  alpine-sdk \
  openssh \
  autoconf \
  tzdata \
  build-base \
  libtool \
  bash \
  automake \
  g++ \
  make \
  && cp /usr/share/zoneinfo/Asia/Bangkok /etc/localtime \
  && echo "Asia/Bangkok" > /etc/timezone

COPY . .

RUN npm i && npm i mssql@6 && npm run build

RUN rm -rf src

CMD ["pm2-runtime", "process.json"]
